\documentclass[11pt,a4paper,titlepage]{article}


\usepackage[utf8]{inputenc}
\usepackage{amssymb,array}
\usepackage{amsmath} 
\usepackage{graphicx}
\usepackage[font=small,format=hang,labelfont={sf,bf}]{caption}
\usepackage{indentfirst}
\usepackage[colorlinks=false]{hyperref}
\usepackage{microtype}
\usepackage{subfig}
\usepackage{wrapfig}
\usepackage{sidecap}
\usepackage{placeins}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{color}
\usepackage{dirtree}

\setlength\parindent{0pt}


\lstset{basicstyle=\scriptsize , stringstyle=\nrmalfamily , keywordstyle=\color{blue}\bfseries}
\begin{document}

\title{\textsf{proto-E2ES\_v1.0} User Manual\\
document version: 1.0}
\author{Erik Romelli - \color{cyan}{romelli@oats.inaf.it}\color{black}}
\date{04 February 2016}
\maketitle


\section*{Scope of the document}

This document is meant to give a detailed description of the functioning and usage of the \textsf{proto-E2ES\_v2.0} and to give some tips about the installation of the simulator itself and its dependencies. This document is meant to be a simple (at least as simple as possible) guide to help the user to use our simulator without issues or big troubles. If he/she encounters some problems during the simulation procedure, even after reading this document, please do not hesitate to contact me at my address: see title page.

\newpage

\section*{List of module acronyms}

\begin{itemize}
	\item[] \textbf{ECSS}: EuClid Survey Strategy
	\item[] \textbf{SS}: Simulated Sky
	\item[] \textbf{SCE}: SpaceCraft \& Environment
	\item[] \textbf{OM}: Optical Model
	\item[] \textbf{DS}: Detection System
	\item[] \textbf{OBDG}: On-Board Data Generation
	\item[] \textbf{DPC}: Data Processing \& Calibration
	\item[] \textbf{PA}: Performance Assessment	
\end{itemize}

\newpage

\tableofcontents

\newpage

\section{Download}

The simulator can be downloaded from my Bitbucket profile in the \href{https://bitbucket.org/Dottorando/proto-e2es_v2.0/downloads}{downloads section}. The simulator is compressed in \textsf{.zip} format. The file contains the parent directory of the simulator: all the source code and the auxiliary files needed by the simulator are in this folder.

\newpage

\section{Installation}

The source code of the simulator is written in \textsf{Python} and a real installation is not needed. The used \textsf{Python} version is 2.7.6. This version of the simulator needs the external software \textsf{TIPS} and some \textsf{Python} libraries (see section \ref{dependencies}). A bash script is provided for an automatic installation of \textsf{TIPS} on the LODEEN virtual machine. In this section we deal with the LODEEN example. If the user is interested in installing the simulator on any other Linux based operating system, some suggestions on the manual installation of \textsf{TIPS} are provided in the appendice \ref{appA} of the document.\\ 

Once the simulator is downloaded the user must decompress it:

\begin{lstlisting}[language=bash]
$ unzip Dottorando-proto-e2es_v2.0-ba574701d38a.zip
\end{lstlisting}

Since the installer refers to the parent directory as the \textsf{proto-E2ES\_v2.0} directory, we suggest the user to rename it:

\begin{lstlisting}[language=bash]
$ mv Dottorando-proto-e2es_v2.0-ba574701d38a proto-E2ES_v2.0
\end{lstlisting}

To let the installer find the correct paths the user must copy or move the installer outside the  \textsf{proto-E2ES\_v2.0} directory:

\begin{lstlisting}[language=bash]
$ mv LODEEN_installer.sh .
\end{lstlisting}

Launching the installer:

\begin{lstlisting}[language=bash]
$ sh LODEEN_installer.sh
\end{lstlisting}

\textsf{TIPS} and its dependencies will be installed. The last step is to install if not already installed, the needed \textsf{Python} libraries. The needed \textsf{Python} libraries are listed in section \ref{dependencies}. To install any \textsf{Python} library follow the following procedure:

\begin{lstlisting}[language=bash]
$ sudo yum install python-pip
$ sudo pip install [ library ]
\end{lstlisting}
 
To run the simulator we have to set some environmental variables to let the SW find all the dependencies in the right place and to run some SW tools properly. The path of the libraries are automatically set by the bash script and changed accordingly to the local installation in the \textsf{.source} file we can find in the parent directory. To set the environment, the user just have to type, in the parent directory:

\begin{lstlisting}[language=bash]
$ source set_environment.source
\end{lstlisting}

Now the simulator can be launched simply typing:

\begin{lstlisting}[language=bash]
$ python proto-E2ES_v2.0.py
\end{lstlisting}

\newpage

\section{proto-E2ES\_v1.0}

In this section we describe the organization of the directories contained in the parent directory proto-E2ES\_v2.0, which is summarized in the chart below:\\

\dirtree{%
.1 proto-E2ES\_v2.0.
.2 auxiliary.
.3 axe.
.3 Euclid\_Mission\_DB.
.3 redshift\_extraction.
.3 Performance\_Assessment\_Module.
.3 ZEUS.
.2 catalog.
.3 images.
.4 000.
.5 \color{blue}{image\_1.fits}.
.5 \color{blue}{...}.
.5 \color{blue}{image\_320.fits}.
.3 incident.
.4 000.
.5 \color{blue}{incident\_1.fits}.
.5 \color{blue}{...}.
.5 \color{blue}{incident\_320.fits}.
.3 \color{blue}{data.fits}.
.2 conf.
.2 doc.
.3 UserManual.
.2 src.
.3 aXe\_executable.
.3 python.
.2 \color{blue}{GlobalConfigurationParameters.xml}.
.2 \color{blue}{OperationalTimeline.csv}.
.2 \color{blue}{proto-E2ES\_v2.0.py}.
}
-

The black objects in the chart are directories, while the blue ones are files. A brief description of each object is given in the following subsections. Some other directories and files are created by the simulator during the simulation by the different modules and are described in the dedicated sections.

\subsection{auxiliary}

This directory contains auxiliary files used by the simulator. In these files some parameters used by different modules are stored. If, in the future, one or some of the parameters will change, the files in this directory will be modified accordingly. The list of the auxiliary files and their version is given below:\\

axe
\begin{itemize}
  \item[-] DithersRot.csv\\
  version 1.0\\
  containing the information on the dither rotation pattern of the field of view;
  \item[-] Filters.csv\\
  version 1.0\\
  containing the information on the NISP filters;
  \item[-] aXeRun.sh\\
  version 1.0\\
  containing the sequence of \textsf{aXe} executables to be used to extract the spectra from \textsf{TIPS} slitless images. This bash script is launched by the DPC module to run \textsf{aXe}.
\end{itemize}

Euclid\_Mission\_DB
\begin{itemize}
  \item[-] Contains a preliminary version of the Euclid mission database. The current version is 1.6.11.
\end{itemize}

Performance\_Assessment\_Module
\begin{itemize}
	\item[-] scientifc\_requirements.csv\\
	version 1.0\\
	contains the scientific requirement on redshift completeness, purity and redshift error.
\end{itemize}

redshift\_extraction
\begin{itemize}
  \item[-] Righe.dat\\
  version 1.1\\
  contains the lines to look for in order to measure the redshift;
\end{itemize}

ZEUS
\begin{itemize}
	\item[-] zody\_corot\_mean\_nside=64\_w=1.25.fits\\
	version 1.0\\
	contains Zody maps at $1.25~\mu\text{m}$.
	\item[-] zody\_corot\_mean\_nside=64\_w=2.20.fits\\
	version 1.0\\
	contains Zody maps at $2.20~\mu\text{m}$.
	\item[-] zody\_corot\_mean\_nside=64\_w=3.50.fits\\
	version 1.0\\
	contains Zody maps at $3.50~\mu\text{m}$.
\end{itemize}

\subsection{catalog}

Contains the catalog file \color{blue}{data.fits} \color{black} and the thumbnails of images and spectra for all the sources in the same catalog, organized as shown in the chart.

\subsection{conf}

Contains configuration files of the different tools used by the simulator. Before the simulation is run it just contains the \color{blue}{tips\_configuration.fits} \color{black} file. During the simulation other configuration files are created and stored in this directory (see subsections \ref{DS} and \ref{DPC}).

\subsection{doc}

Contains documentation related to the simulator. In the subdirectory UserManual the \textsf{.tex} source of this user manual, the \textsf{.pdf} version and all the \LaTeX\ products are stored.

\subsection{src}

Contains the executable files of \textsf{aXe} and dedicated \textsf{Python} libraries. The list of contained files follows:\\

aXe\_executables

\begin{itemize}
  \item[-] aXe\_AF2PET, version 3.16; 
  \item[-] aXe\_GOL2AF, version 3.16;
  \item[-] aXe\_PET2SPC, version 3.16;
  \item[-] aXe\_PETCONT, version 3.16;
  \item[-] aXe\_PETFF, version 3.16;
  \item[-] aXe\_SEX2GOL, version 3.16;
\end{itemize}

python

\begin{itemize}
  \item[-] axe\_files.py, version 2.0;
  \item[-] catalog\_handler.py, version 2.0;
  \item[-] Mdb.py, version 1.6.11
  \item[-] performance\_assessment.py, version 2.0;  
  \item[-] SimNISP.py, version 1.1;		
  \item[-] spectral\_tools, version 1.0.
\end{itemize}

\subsection{files}

The parent directory also contains:

\begin{itemize}
	\item[-] \color{blue}{GlobalConfigurationParameters.xml}\color{black}: see section \ref{conffile};
	\item[-] \color{blue}{OperationalTimeline.csv}\color{black}: see section \ref{ECSS};
	\item[-] \color{blue}{proto-E2ES\_v2.0.py}\color{black}: the proper simulator script, see \ref{usage}.
\end{itemize}

\newpage

\section{Usage}\label{usage}

\textsf{proto-E2ES\_v2.0} is really easy to use. To launch a simulation, in the parent directory, the user has to type via command line:

\begin{lstlisting}[language=bash]
$ python proto-E2ES_v2.0
\end{lstlisting}

To set-up the simulation, the user just has to change the values of the variables in the \textsf{GlobalConfigurationParameters.xml} file to switch on or off some modules or change some configuration parameters. A detailed description of this file is given in the following subsection. 

\subsection{Overall configuration}\label{conffile}

In this subsection we give a description of the different options in the \textsf{GlobalConfigurationParameters.xml} file and we list the allowed values that can be assigned to each option. The file is provided in its version 1.1.\\

The configuration parameters in the file are organized in four different categories:

\begin{itemize}
	\item[-] FileNames: contains the names of the files the simulator has to be read or written by the simualator;
	\item[-] DirectoryNames: contains the names of the directories to be accessed or created by the simualator;
	\item[-] SimualtionParameters: contains overall parameters to be used during the simulation;
	\item[-] Modules: contains the information on which modules have to be used by the simulator.	
\end{itemize}

For each parameter listed in the configuration file the user can find the following entries:

\begin{itemize}
	\item[-] \textbf{Parameter}: parameter name searched by the code;
	\item[-] \textbf{Description}: brief description of the parameter;
	\item[-] \textbf{Comment}: brief comment on the usage of the parameter;
	\item[-] \textbf{Source}: [only FileNames] directory in which the file is stored;
	\item[-] \textbf{Release}: [only FileNames] version release of the file;
	\item[-] \textbf{Format}: [only FileNames] file format;
	\item[-] \textbf{Value}: value of the parameter, \textit{i.e.} name of the file or directory, numerical or boolean value.
\end{itemize}

Specific parameters are summarized in tables \ref{FileNames}, \ref{DirectoryNames}, \ref{SimulationParameters}, \ref{Modules}.

\newpage

\begin{table}[h!]\centering
\caption{\textbf{File Names}}\label{FileNames}
\resizebox{\textwidth}{!}{
\begin{tabular}{l|l|l|l|l|l}
\textbf{Parameter} & \textbf{Description} & \textbf{Source} & \textbf{Release} & \textbf{Format} & \textbf{Value} \\
\hline
&&&&&\\
Catalog & Catalog file & ./catalog & 1.0 & fits & data.fits \\
&&&&&\\
TIPSConfiguration & Configuration file for TIPS & ./conf & 1.0 & fits & tips\_configuration.fits \\
&&&&&\\
OperationalTimeline & Information on the pointings & ./ & 1.0 & csv & OperationalTimeline.csv \\
&&&&&\\
LogFile & Simulation log file & ./ & 2.0 & log & SimulationStatus.log \\
\end{tabular}}
\end{table}

\begin{table}[h!]\centering
\caption{\textbf{Directory Names}}\label{DirectoryNames}
\resizebox{\textwidth}{!}{
\begin{tabular}{l|l|l}
\textbf{Parameter} & \textbf{Description} & \textbf{Value} \\
\hline
&&\\
E2ESOutputDirectory & E2ES output directory & E2ES\_output \\
&&\\
TIPSOutputDirectory & \textsf{TIPS} output directory & TIPS\_output \\
&&\\
PerformanceAssessmentOutputDirectory & PA Module output directory & Performance\_Assessment\_output \\
&&\\
StoringDirectory & Directory for data storing & /path/to/proto-E2ES\_v2.0\_STORING \\
\end{tabular}}
\end{table}

\begin{table}[h!]\centering
\caption{\textbf{Simulation Parameters}}\label{SimulationParameters}
\resizebox{\textwidth}{!}{
\begin{tabular}{l|l|l|l}
textbf{Parameter} & \textbf{Description} & \textbf{Comment} & \textbf{Value} \\
\hline
&&&\\
Pointing & Pointings to be simulated & Write multiple indexes comma separated & 1,2,3,4 \\
&&&\\
Detector & Detectors to be simulated & Write multiple indexes comma separated & 00,01,...,33 \\
&&&\\
CleanAll & Cleans the environment & If T the environment is cleaned & T \\
\end{tabular}}
\end{table}

\begin{table}[h!]\centering
\caption{\textbf{Modules}}\label{Modules}
\resizebox{\textwidth}{!}{
\begin{tabular}{l|l|l|l}
textbf{Parameter} & \textbf{Description} & \textbf{Comment} & \textbf{Value} \\
\hline
&&&\\
ECSS & Euclid Survey Strategy Module & If T the module is used & T \\
&&&\\
SS & Simulated Sky Module & If T the module is used & T \\
&&&\\
SCE & Spacecraft and Environment Module & If T the module is used & F \\
&&&\\
OM & Optical Model Module & If T the module is used & F \\
&&&\\
DS & Detection System & If T the module is used & T \\
&&&\\
OBDG & On-Board Data Generation Module & If T the module is used & F \\
&&&\\
DPC & Data Processing and Calibration Module & If T the module is used & T \\
&&&\\
PA & Performance Assessment Module & If T the module is used & T \\
\end{tabular}}
\end{table}

\newpage

\section{The script}

The whole simulation is performed and handled by the main script \textsf{proto-E2ES\_v2.0.py}. The script performs the following operations:

\begin{itemize}
	\item[-] import \textsf{Python} libraries;
	\item[-] delete old \textsf{.log} file (if present);
	\item[-] read the overall configuration and open a new \textsf{.log};
	\item[-] clean all files and directories of previous simulations$^*$;
	\item[-] run the \textsf{Euclid Survey Strategy Module}$^*$ (see subsection \ref{ECSS});
	\item[-] run the \textsf{Simulated Sky Module}$^*$ (see subsection \ref{SS});
	\item[-] run the \textsf{Spacecraft and Environment Module}$^*$ (see subsection \ref{SCE});
	\item[-] run the \textsf{Optical Model Module}$^*$ (see subsection \ref{OM});
	\item[-] run the \textsf{Detection System Module}$^*$ (see subsection \ref{DS});
	\item[-] run the \textsf{On-Board Data Generation Module}$^*$ (see subsection \ref{OBDG});
	\item[-] run the \textsf{Data Proccessing and Calibration Module}$^*$ (see subsection \ref{DPC});
	\item[-] run the \textsf{Performance Assessment Module}$^*$ (see subsection \ref{PA}).
\end{itemize}

In the following subsections, after each step, we report a chart explaining the products made at each step. The new files and directories are represented in red. At the beginning of the simulation, a directory for each simulated pointing is created within the storing directory defined in the global configuration. In each pointing directory the simulator will store the output of TIPS and the output of the simulator itself:\\

\dirtree{%
.1 path.
.2 to.
.3 proto-E2ES\_v2.0\_STORING.
.4 \color{red}{pointing1}.
.5 \color{red}{E2ES\_output}.
.5 \color{red}{TIPS\_output}.
.4 \color{red}{pointing2}.
.5 \color{red}{E2ES\_output}.
.5 \color{red}{TIPS\_output}.
.4 ....
}
-

In the following subsections we briefly describe the operations of each module. \\

$^*$\footnotesize{This operation is performed according to the option in the overall configuration file.}
\normalsize

\newpage

\subsection{\textsf{Euclid Survey Strategy Module~(ECSS)}}\label{ECSS}

This module loads and reads the operational time-line \textsf{OperationalTimeline.csv}. The simulator takes information about the pointing, the exposure times and some information that we plan to use to simulate the Zody profile with the ZEUS (Zody EUclid Simulator, by M. Maris, INAF-OATs) software in a future version of the simulator. The operational time-line contains the information listed in table \ref{OPT}. The \textsf{OperationalTimeline.csv} file can be edited using a common \textsf{.csv} visualization software, such as \textsf{Excell} or \textsf{OpenOffice Calc}.

\begin{table}
\caption{Structure of \textsf{OperationalTimeline\_lite.csv}.}\label{OPT}
\begin{tabular}{l|c|l}
\hline
\hline
\textbf{Parameter} & \textbf{Value} & \textbf{Description} \\
\hline
pointingID & 1 & Index of the simulated pointing \\
ra0 & 150.72 & Starting value for RA \\
dec0 & 1.04 & Starting value for DEC \\
exptime & 560.0 & Exposure time \\
band & J & IR band of spectra extraction \\
ZLE\_wavelenght & 1.25 & Wavelenght (in $\mu$m) of ZEUS simulation \\
min\_day & 30.0 & Min day simulated by ZEUS \\
max\_day & 35.0 & Max day simulated by ZEUS \\
elong\_min & 40.0 & Min elongation (in deg) simulated by ZEUS \\
elong\_max & 140.0 & Min elongation (in deg) simulated by ZEUS \\
\hline
\hline
\end{tabular}
\end{table}

\subsection{\textsf{Simulated Sky Module~(SS)}}\label{SS}

In this version of the simulator this module creates a \textsf{TIPS} compliant catalog. This module will also handle \textsf{ZEUS}, the Zodiacal Light Emission~(ZLE) simulator (see section \ref{dependencies}), in future versions of the code. The integration of \textsf{ZEUS} in \textsf{proto-E2ES\_v2.0} is still under development.\\

\dirtree{%
.1 catalog.
.2 images.
.2 incident.
.2 \color{blue}{data.fits}.
.2 \color{red}{data\_TIPS.fits}.
}
-

\subsection{\textsf{Spacecraft and Environment Module~(SCE)}}\label{SCE}

This module is not implemented in this version of the simulator. If the user sets the ``Value'' entry of the SCE parameter in the configuration file to ``T'':

\begin{lstlisting}[escapechar=|]
<Parameter title="SCE">
	...
	<Value>|\color{red}{T}|</Value>
	...
</Parameter>
\end{lstlisting}

the simulator just jumps this step.\\

\subsection{\textsf{Optical Model Module~(OM)}}\label{OM}

This module is not implemented in this version of the simulator. If the user sets the ``Value'' entry of the OM parameter in the configuration file to ``T'':

\begin{lstlisting}[escapechar=|]
<Parameter title="OM">
	...
	<Value>|\color{red}{T}|</Value>
	...
</Parameter>
\end{lstlisting}

the simulator just jumps this step. The optical configuration is given with the detector configuration in the \textsf{Detection System Module}. In the next version we hope to be able to create the \textsf{TIPS} configuration file directly from the Euclid database as described in subsection \ref{DS}.\\

\subsection{\textsf{Detection System Module~(DS)}}\label{DS}

This module runs \textsf{TIPS}, on which some information are given in section \ref{dependencies}. The \textsf{TIPS} simulation is performed taking in input the \textsf{TIPS} compliant catalog (\textsf{data\_TIPS.fits}) created by the \textsf{Simulated Sky Module} and a custom configuration file (\textsf{tips\_configuration.fits}) containing the optical and instrumental set-up. The simulator stores the log of the \textsf{TIPS} run in a dedicated file: \textsf{TIPS\_output.log}. We leave the description of the proper output of \textsf{TIPS} to the \href{http://www.cppm.in2p3.fr/renoir/IMG/pdf/EUCLID_CPPM_SGS_TIPS_Prototype_Description-v2-3.pdf}{documentation} of \textsf{TIPS} itself.\\

\dirtree{%
.1 path.
.2 to.
.3 proto-E2ES\_v2.0\_STORING.
.4 pointing1.
.5 E2ES\_output.
.5 TIPS\_output.
.6 \color{red}{CONF}.
.6 \color{red}{DATA}.
.6 \color{red}{DRIZLE}.
.6 \color{red}{OUTPUT}.
.6 \color{red}{OUTSIM}.
.6 \color{red}{SIMDATA}.
.6 \color{red}{TIPS\_output.log}.
.4 pointing2.
.4 ....
}

In the next versions of the simulator, we want to integrate the possibility to navigate the mission database, via the \textsf{Mdb.py} API, designed specifically for this purpose. In future, we hope to be able to create the \textsf{TIPS} configuration file directly from the Euclid mission database.\\

\subsection{\textsf{On-Board Data Generation Module~(OBDG)}}\label{OBDG}

This module is not implemented in this version of the simulator. If the user sets the ``Value'' entry of the OBDG parameter in the configuration file to ``T'':

\begin{lstlisting}[escapechar=|]
<Parameter title="OBDG">
	...
	<Value>|\color{red}{T}|</Value>
	...
</Parameter>
\end{lstlisting}

the simulator just jumps this step.

\subsection{\textsf{Data Processing and Calibration Module~(DPC)}}\label{DPC}

This module simulates the ground-based data processing. The sequence of operations made in this module is summarized below, enlightening the products of each step. \\

First of all, the simulator extracts via \textsf{aXe} the 2D spectra of the different sources in the simulated field. \textsf{aXe} needs some configuration files which are created and stored in the \textsf{conf} directory:\\

\dirtree{%
.1 proto-E2ES\_v1.0.
.2 ....
.2 conf.
.3 \color{red}{axe\_dither0.conf}.
.3 \color{red}{axe\_dither1.conf}.
.3 \color{red}{axe\_dither2.conf}.
.3 \color{red}{axe\_dither3.conf}.
.3 tips\_configuration.fits.
.3 \color{red}{tips\_configuration\_SENS\_GRED\_realest\_A.fits}.
.2 doc.
.2 src.
.2 ....
}
-

For each detector \textsf{XX} the simulator creates a directory called \textsf{DETECTOR\_XX} within the simulator output directory, defined in the overall configuration file. For each source the simulator look for emission lines in the \textsf{aXe} extracted spectra and tries to perform the redshift measures. \\

\dirtree{%
.1 path.
.2 to.
.3 proto-E2ES\_v2.0\_STORING.
.4 pointing1.
.5 E2ES\_output.
.6 \color{red}{DETECTOR\_00}.
.6 \color{red}{DETECTOR\_01}.
.6 ....
.6 \color{red}{DETECTOR\_33}.
.5 TIPS\_output.
.4 pointing2.
.4 ....
}
-\\

All the measures are stored, per pointing, in a \textsf{.fits} file, within the pointing directory.\\

\dirtree{%
.1 path.
.2 to.
.3 proto-E2ES\_v2.0\_STORING.
.4 pointing1.
.5 E2ES\_output.
.5 TIPS\_output.
.5 \color{red}{RESULTS.redshift\_measures.fits}.
.4 pointing2.
.4 ....
}

\subsection{\textsf{Performance Assessment Module~(PA)}}\label{PA}

In this module the script collects all the measures done in the \textsf{Data Processing andCalibration Module} and creates a set of plots and a table containing some statistics on the simulation, in order to assess the performance of the simulated chain. The plots, stored in the PA output directory, as indicated in the global configuration file, are:

\begin{itemize}
	\item[-] \textsf{RESULTS.plot\_hit.png}\\
	showing which sources have been treated by the simulator at each simulated pointing;
	\item[-] \textsf{RESULTS.plot\_matches.png}\\
	showing how many line matches have been obtained in each redshift measurement;
	\item[-] \textsf{RESULTS.plot\_pointing.png}\\
	showing how the redshift measurements are divided in the different simulated pointing;
\end{itemize}

In the \textsf{RESULTS.Simulation\_Statistics.csv} file, some statistics about the redshift completeness, purity and the redshift errors are reported.\\

\dirtree{%
.1 path.
.2 to.
.3 proto-E2ES\_v2.0\_STORING.
.4 \color{red}{Performance\_Assessment\_output}.
.5 \color{red}{RESULTS.plot\_hit.png}.
.5 \color{red}{RESULTS.plot\_matches.png}.
.5 \color{red}{RESULTS.plot\_pointing.png}.
.5 \color{red}{RESULTS.Simulation\_Statistics.csv}.
.4 pointing1.
.4 pointing2.
.4 ....
}
-\\

This module is implemented in a preliminary version. 

\newpage

\section{The \textsf{.log} file}

In the \textsf{SimulatorStatus.log} file the simulator reports its progress. First of all it stores the information of the overall configuration file and then it monitors the functioning of the simulator module by module. In table \ref{log} the structure of the \textsf{.log} file is reported. Note that the most verbose steps are summarized by comments in capital red.

\begin{table}\centering 
\caption{Scheme of the structure of \textsf{SimulatorStatus.log}.}\label{log}
\begin{tabular}{c}
\hline
\hline
\begin{lstlisting}[escapechar=|]
Read configuration file -> Done

GlobalParameters.dat content:

#########################################
|\color{red}{SUMMARY OF GlobalConfigurationParameters.xml}|
#########################################

Cleaninig (All)...
Clean directories...
|\color{red}{CLEANS DIRECTORIES}|
All directories cleaned
Clean files...
|\color{red}{CLEANS FILES}|
All files cleaned
Cleaninig complete

Create ./E2ES_output...

Run Euclid_Survey_Strategy_Module...
Euclid_Survey_Strategy_Module -> Done

Run Simulated_Sky_Module...
Simulated_Sky_Module -> Done

Run Spacecraft_&_Environment_Module...
Spacecraft_&_Environment_Module -> Done

Run Optical_Model_Module...
Optical_Model_Module -> Done

Run Detection_System_Module...
Detection_System_Module_Module -> Done

Run On-Board_Data_Generation_Module...
On-Board_Data_Generation_Module -> Done

Run Data_Processing_&_Calibration_Module...
|\color{red}{CLEANING...}|
Creating aXe configuration file...
Creating ./E2ES_output/DETECTOR_00/...
|\color{red}{RUNNING AXE}|
Extracting z...
Data_Processing_&_Calibration_Module -> Done

Run Performance_Assessment_Module...
Creating Performance_Assessment_output (if not present)...
Plotting...
Evaluating statistics...
Performance_Assessment_Module -> Done

And that's all folks! |\color{red}{END OF THE FILE}|
\end{lstlisting}\\
\hline
\hline
\end{tabular}
\end{table}

\newpage

\section{Dependencies}\label{dependencies}

The correct functioning of the \textsf{proto-E2ES\_v2.0} depends on some libraries that the user shall install previously. We can divide the \textsf{proto-E2ES\_v2.0} dependencies in two groups: common \textsf{Python} libraries and specific SW tools.\\

The common \textsf{Python} libraries needed by the \textsf{proto-E2ES\_v2.0} are:

\begin{itemize}
	\item \textsf{numpy}, version 1.9.2$^*$
	\item \textsf{matplotlib}, version 1.3.1$^*$
	\item \textsf{astropy}, version 1.0.3$^*$
\end{itemize} 

The installation of this libraries is easy and can be done in two ways:

\begin{itemize}
\item[]\textbf{method 1}:

\begin{lstlisting}[escapechar=|]
$ sudo apt-get install python-pip
$ sudo pip install [library]
\end{lstlisting}
on Debian based systems, otherwise:
\begin{lstlisting}[escapechar=|]
$ sudo yum install python-pip
$ sudo pip install [library]
\end{lstlisting}

\item[]\textbf{method 2}:

\begin{lstlisting}[language=bash]
$ sudo apt-get install python-[library]
\end{lstlisting}
only on Debian based systems.
\end{itemize}

The SW tools used to implement the simulator are:

\begin{itemize}
	\item \textsf{ZEUS}: Zody EUclid Simulator, a tool to simulate Zodiacal Light Emission~(ZLE). Written by Michele Maris\footnote{INAF - Osservatorio Astronomico di Trieste, e-mail: maris@oats.inaf.it};
	\item \textsf{Mdb.py}: a \textsf{Python} API designed to navigate the Euclid mission database\footnote{Not integrated in this version of the simulator.};
	\item \textsf{aXe} executables. \textsf{aXe} is designed for the extraction, calibration, visualization, and simulation of spectra from slit-less spectroscopic instruments. \textsf{aXe} is under the responsability of the Space Telescope Science Institute~(STScI)\footnote{http://axe-info.stsci.edu/}.
\end{itemize}

$^*$\footnotesize{The package manager install the latest version.}
\normalsize

\newpage
\appendix

\section{TIPS installation}\label{appA}

\textsf{TIPS} is one of the main dependencies of the \textsf{proto-E2ES\_v2.0}. It is the core of the \textit{Detection System Module} and it is the SW tool that simulates the NISP-S detectors. \textsf{TIPS} has its own documentation, however we report in this manual a brief description about its installation. For further information on \textsf{TIPS} we suggest to look at the  \href{http://www.cppm.in2p3.fr/renoir/IMG/pdf/EUCLID\_CPPM\_SGS\_TIPS\_Prototype\_Description-v2-3.pdf}{\textsf{TIPS} Description Document}\\

\textsf{TIPS} has its own dependencies. To run properly it needs:

\begin{itemize}
	\item \textsf{gcc} version $\geq 2.95$
	\item \textsf{gsl} version $\geq 1.0$
	\item \textsf{wcstools} version $\geq 3.6$
	\item \textsf{cfitsio} version $\geq 2.0$	
\end{itemize} 

Usually the issues arise with the \textsf{wcstools} and \textsf{cfitsio} libraries, for that reason we report an installation example in which we need to previously install them\footnote{We suggest a local installation of the libraries, because it is easier to handle the installation path.}.\\

To correctly install \textsf{TIPS} and its dependencies follow the following scheme (the same scheme is implemented in \textsf{LODEEN\_installer.sh}):

\begin{itemize}
	\item Download and install \textsf{wcstool}:
\begin{lstlisting}[language=bash]
$ wget http://tdc-www.harvard.edu/software/wcstools/wcstools-3.9.2.tar.gz
$ tar -xvf wcstools-3.9.2.tar.gz
$ cd wcstools-3.9.2
$ make
\end{lstlisting}
	\item Download, from \href{http://heasarc.gsfc.nasa.gov/docs/software/fitsio/}{this link}, and install \textsf{cfitsio}:
\begin{lstlisting}[language=bash]
$ tar -xvf cfitsio3370.tar.gz
$ cd cfitsio
$ ./configure
$ make
$ make install
\end{lstlisting}
	\item Download and install \textsf{TIPS}:
\begin{lstlisting}[language=bash]
$ svn checkout http://svn.oamp.fr/repos/tips/branches/tips_codeen
$ cd tips_codeen/axesim/
$ ./configure --with-cfitsio-prefix=your/path/to/cfitsio
              --with-wcstools-prefix=your/path/to/wcstools-3.9.2
$ make
\end{lstlisting}
\end{itemize}

After the installation to correctly run \textsf{TIPS} you have to export the path to the \textsf{TIPS} python libraries:

\begin{lstlisting}[language=bash]
$ export PYTHONPATH=$PYTHONPATH:/your_local_path/tips_axesim/
$ export PYTHONPATH=$PYTHONPATH:/your_local_path/tips_axesim/axesim
$ export PYTHONPATH=$PYTHONPATH:/your_local_path/tips_axesim/axesim/Library
\end{lstlisting}

Remember to add your local path to the \textsf{TIPS} python libraries in the file \textsf{set\_environment.source} in the \textsf{proto-E2ES\_v2.0} directory, so you can use \textsf{TIPS} without any issue running your simulation.\\

If your installation ended successfully you can try the correct functioning of tips entering in \textsf{Python} and typing:

\begin{lstlisting}[language=python]
>> import test_tips
>> test_tips.run()
\end{lstlisting}

\subsection{Possible issues}

In this subsection we report some issues that the user can encounter during the installation.

\begin{itemize}
	\item \textsf{svn command not found}. The user must install the \textsf{subversion} package:
\begin{lstlisting}[escapechar=|]
$ sudo apt-get install subversion
\end{lstlisting}
in Debian based systems, or:
\begin{lstlisting}[escapechar=|]
$ sudo yum install subversion
\end{lstlisting}
	\item \textsf{gsl is missing}. The user must install it. We suggest the following procedure:
\begin{lstlisting}[language=bash]
$ wget http://ftp.gnu.org/pub/gnu/gsl/gsl-1.16.tar.gz
$ tar -xvf gsl-1.16.tar.gz
$ cd gsl-1.16
$ ./configure
$ make
$ sudo make install
\end{lstlisting}
	\item \textsf{asciidata} is missing. The user must install it. We suggest the following procedure, after the download of \textsf{asciidata-1.1.1.tar.gz} from \href{http://www.stecf.org/software/PYTHONtools/astroasciidata/asciidata_download.php}{here}:
\begin{lstlisting}[language=bash]
$ tar -xvf asciidata-1.1.1.tar.gz
$ cd asciidata-1.1.1
$ sudo python setup.py install
\end{lstlisting}
\end{itemize}

\newpage

\begin{center}
\huge{End of the document.}
\end{center}



\end{document}