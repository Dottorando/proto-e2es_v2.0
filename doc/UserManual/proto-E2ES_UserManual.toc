\contentsline {section}{\numberline {1}Download}{4}{section.1}
\contentsline {section}{\numberline {2}Installation}{5}{section.2}
\contentsline {section}{\numberline {3}proto-E2ES\_v1.0}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}auxiliary}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}catalog}{8}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}conf}{8}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}doc}{8}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}src}{8}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}files}{9}{subsection.3.6}
\contentsline {section}{\numberline {4}Usage}{10}{section.4}
\contentsline {subsection}{\numberline {4.1}Overall configuration}{10}{subsection.4.1}
\contentsline {section}{\numberline {5}The script}{12}{section.5}
\contentsline {subsection}{\numberline {5.1}\textsf {Euclid Survey Strategy Module\nobreakspace {}(ECSS)}}{13}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}\textsf {Simulated Sky Module\nobreakspace {}(SS)}}{13}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}\textsf {Spacecraft and Environment Module\nobreakspace {}(SCE)}}{13}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}\textsf {Optical Model Module\nobreakspace {}(OM)}}{14}{subsection.5.4}
\contentsline {subsection}{\numberline {5.5}\textsf {Detection System Module\nobreakspace {}(DS)}}{14}{subsection.5.5}
\contentsline {subsection}{\numberline {5.6}\textsf {On-Board Data Generation Module\nobreakspace {}(OBDG)}}{15}{subsection.5.6}
\contentsline {subsection}{\numberline {5.7}\textsf {Data Processing and Calibration Module\nobreakspace {}(DPC)}}{15}{subsection.5.7}
\contentsline {subsection}{\numberline {5.8}\textsf {Performance Assessment Module\nobreakspace {}(PA)}}{16}{subsection.5.8}
\contentsline {section}{\numberline {6}The \textsf {.log} file}{18}{section.6}
\contentsline {section}{\numberline {7}Dependencies}{20}{section.7}
\contentsline {section}{\numberline {A}TIPS installation}{21}{appendix.A}
\contentsline {subsection}{\numberline {A.1}Possible issues}{22}{subsection.A.1}
