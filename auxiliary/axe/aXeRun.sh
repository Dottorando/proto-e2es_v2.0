#!/bin/bash

in_fits=$1
in_dat=$2
in_conf=$3

export aXe_bin=./src/aXe_executable

# clean the environment
# 1. sex2gol 
# => INPUT_OUT.cat
${aXe_bin}/aXe_SEX2GOL $in_fits $in_conf -in_SEX=$in_dat -no_direct_image

# 2. gol2af 
# => INPUT_OUT.OAF
${aXe_bin}/aXe_GOL2AF $in_fits $in_conf -orient=0 -mfwhm=1.0

# 3. af2pet 
# => INPUT_OUT.PET.fits
${aXe_bin}/aXe_AF2PET $in_fits $in_conf

# 3. petcont 
# => (modifica) INPUT_OUT.PET.fits
# => INPUT_OUT.CONT.fits
${aXe_bin}/aXe_PETCONT $in_fits $in_conf -cont_model=4 -cont_map=1 -lambda_psf=1220.0

# 5. petff 
# => senza FF probabilmente non fa nulla
${aXe_bin}/aXe_PETFF $in_fits $in_conf --back=0

# 6. pet2spc 
# => INPUT_OUT.SPC.fits
${aXe_bin}/aXe_PET2SPC $in_fits $in_conf -noBPET

    