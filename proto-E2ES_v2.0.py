'''
 *
 * Copyright (C) 2015 Euclid Science Ground Segment
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
 * any later version.
 *
 * This script is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 *
 *   Author:
 *
 *   	Erik Romelli
 *   
 *   	PhD Student
 *
 *   	University of Trieste, via Valerio 2, Trieste, Italy
 *   	INAF-OATS, via Bazzoni 2, Trieste, Italy
 *   	room 123
 *   	mail: romelli@oats.inaf.it
 *   	tel: +39 040 3199 123
 *   	cell: +39 340 8991293
 *
 *   Last modified: 02/02/2016
 *   Version: 2.0
 *
'''

# Import standard libraries
import sys
import os
import shutil
import time
#import csv
import subprocess
import tempfile
import numpy as np
import xml.etree.cElementTree as xmlET
from astropy.io import fits

# Import dedicated ibraries (QUESTE CAMBIERANNO UN BEL PO')
from catalog_handler import CatalogHandler
from SimNISP import NISP_S
from spectral_tools import SpectralTools
from axe_files import aXeFiles
from performance_assessment import PerformanceAssessment

#from axe_files import FileGenerator
#from fits_handler import split_components,create_tips_input_catalog, spec2mono, combine_spectra
#from performance_assessment import PerformanceAssessment

print 'Engine start'

# global settings file name: pay attention editing these variable
global_configuration_parameters = 'GlobalConfigurationParameters.xml'

# =================== Read the overall configuration ==========================================================

try:  

  print 'Reading global configuration from %s...'%global_configuration_parameters

  GCP=xmlET.parse(global_configuration_parameters)

  # delete .log to be sure the one you find is always the new one
  os.remove(GCP.find("./FileNames/Parameter/[@title='LogFile']/Value").text) if os.path.exists(GCP.find("./FileNames/Parameter/[@title='LogFile']/Value").text) else None

  # Open the .log and write that the global configuration has been read correctly 
  _log = open(GCP.find("./FileNames/Parameter/[@title='LogFile']/Value").text,'w+')
  _log.write('Read configuration file -> Done\n')
  _log.write('\n')
  _log.write('%s content:\n'%global_configuration_parameters)
  _log.write('\n')
  _log.write('####################################################################\n')
  for param in GCP.iter("Parameter"):
    _log.write(param.get('title')+': '+param.find('Value').text+'\n')
  _log.write('####################################################################\n')
  _log.write('\n')

except:

  # delete .log to be sure the one you find is always the new one
  os.remove(GCP.find("./FileNames/Parameter/[@title='LogFile']/Value").text)

  # Open the .log and write that the global configuration has NOT been read correctly 
  _log = open(GCP.find("./FileNames/Parameter/[@title='LogFile']/Value").text,'w+')
  _log.write('Read configuration file -> Failed\n')
  _log.write('Error 404: file %s not found\n'%global_configuration_parameters)
  _log.write('\n')
  _log.close()
  print 'Read configuration file -> Failed'
  print 'An error occured: check the log for further information'
  sys.exit(1)

# define pointings to simulate
pointing_list=np.array(GCP.find("./SimulationParameters/Parameter/[@title='Pointing']/Value").text.split(','),dtype=int)
# define detectors to simulate
detector_list=np.array(GCP.find("./SimulationParameters/Parameter/[@title='Detector']/Value").text.split(','))
# finds the number of sources in the catalog
source_num = fits.open(os.path.join(GCP.find("./FileNames/Parameter/[@title='Catalog']/Source").text,GCP.find("./FileNames/Parameter/[@title='Catalog']/Value").text))[1].header['NAXIS2']

# ============================================================================================================

# =================== Clean directrories and files ===========================================================

if GCP.find("./SimulationParameters/Parameter/[@title='CleanAll']/Value").text == 'T':
  
  print 'Cleaning directories and files from older simulations...'

  _log.write('Cleaninig (All)...\n')
  
  # clean files
  _log.write('Deleting older files...\n')

  to_be_cleaned = [os.path.join(GCP.find("./FileNames/Parameter/[@title='Catalog']/Source").text,'data_TIPS.fits'), \
		   os.path.join(GCP.find("./FileNames/Parameter/[@title='TIPSConfiguration']/Source").text,'tips_configuration_SENS_GRED_realest_A.fits') \
		  ]
  for n in np.arange(4):
    to_be_cleaned.append(os.path.join(GCP.find("./FileNames/Parameter/[@title='aXeConfiguration']/Source").text,'axe_dither%d.conf'))

  for item in to_be_cleaned:
    os.remove(item) if os.path.exists(item) else None
    _log.write('%s -> Cleaned\n'%item)
  
  _log.write('All files deleted...\n')
  to_be_cleaned=[]

  # clean directories
  _log.write('Deleting older directories...\n')
  
  for pointing_id in pointing_list:
    to_be_cleaned.append(os.path.join(GCP.find("./DirectoryNames/Parameter/[@title='StoringDirectory']/Value").text,'pointing%d'%pointing_id))
  
  for item in to_be_cleaned:
    shutil.rmtree(item) if os.path.exists(item) else None
    _log.write('%s -> Cleaned\n'%item)

  _log.write('All directories deleted...\n')

  _log.write('Cleaninig complete\n')
  _log.write('\n')

# =================== Creating storing folder for output storing =============================================

os.mkdir(GCP.find("./DirectoryNames/Parameter/[@title='StoringDirectory']/Value").text) if not os.path.exists(GCP.find("./DirectoryNames/Parameter/[@title='StoringDirectory']/Value").text) else None

# =================== Creating pointing folders for output storing ===========================================

print 'Creating storing directories (if not present)...'

for point_id in pointing_list:
  
  point_folder=os.path.join(GCP.find("./DirectoryNames/Parameter/[@title='StoringDirectory']/Value").text,'pointing%d'%point_id)
  point_folder_E2ESoutput=os.path.join(GCP.find("./DirectoryNames/Parameter/[@title='StoringDirectory']/Value").text,'pointing%d'%point_id,GCP.find("./DirectoryNames/Parameter/[@title='E2ESOutputDirectory']/Value").text)

  os.mkdir(point_folder) if not os.path.exists(point_folder) else None
  os.mkdir(point_folder_E2ESoutput) if not os.path.exists(point_folder_E2ESoutput) else None
  _log.write('Create %s (if not present)...\n'%point_folder)
  _log.write('Create %s (if not present)...\n'%point_folder_E2ESoutput)
  _log.write('\n')

# =================== Read the ECSS Module ===================================================================


if GCP.find("./Modules/Parameter/[@title='ECSS']/Value").text == 'T':
  
  print 'Run Euclid Survey Strategy Module...'

  _log.write('Run Euclid Survey Strategy Module...\n')
  
  # the list below is made to open the csv columns with the right data type
  opt_typelist=[int,float,float,float,'|S5',float,float,float,float,float]
  
  try:
    opt = np.genfromtxt(open(GCP.find("./FileNames/Parameter/[@title='OperationalTimeline']/Value").text,'r'),delimiter=',',names=True,dtype=opt_typelist)
    _log.write('Euclid Survey Strategy Module -> Done\n')
    _log.write('%s correctly loaded\n'%GCP.find("./FileNames/Parameter/[@title='OperationalTimeline']/Value").text)
    _log.write('\n')
    if len(opt) == 0:
      print 'An error occured: check the log for further information'
      _log.write('Euclid Survey Strategy Module -> Failed\n')
      _log.write('Ehi! Nothing to do there, probably your file is empty!\n')
      _log.write('\n')
  except:
    _log.write('Euclid Survey Strategy Module -> Failed\n')
    _log.write('Error 404: file %s not found\n'%GCP.find("./FileNames/Parameter/[@title='OperationalTimeline']/Value").text)
    _log.write('\n')
    print 'An error occured: check the log for further information'
    sys.exit(1)
    
else:

  print 'Skip Euclid Survey Strategy Module...'

  _log.write('Euclid Survey Strategy Module -> Skipped\n')
  _log.write('\n')


# ============================================================================================================

# =================== Read the SS Module =====================================================================

# OSS: questo modulo dovra' gestirsi ZEUS

if GCP.find("./Modules/Parameter/[@title='SS']/Value").text == 'T':

  _log.write('Run Simulated Sky Module...\n')

  try:
    print 'Run Simulated Sky Module...'
    CH=CatalogHandler(GCP.find("./FileNames/Parameter/[@title='Catalog']/Value").text)
    CH.create_TIPS_input_catalog()
    _log.write('Simulated Sky Module -> Done\n')
    _log.write('Catalog correctly prepared for TIPS\n')
    _log.write('\n')
  except:
    _log.write('Simulated Sky Module -> Failed\n')
    _log.write('Error: check if file names or paths are set properly\n')
    _log.write('\n')    
    print 'An error occured: check the log for further information'

else:

  print 'Skip Simulated Sky Module...'

  _log.write('Simulated Sky Module -> Skipped\n')
  _log.write('\n')

# ============================================================================================================

# =================== Read the SCE Module ====================================================================

# Questo per ora e' un modulo fantasma

if GCP.find("./Modules/Parameter/[@title='SCE']/Value").text == 'T':

  print 'Run Spacecraft & Environment Module...'

  _log.write('Run Spacecraft & Environment Module...\n')
  
  print 'In this Universe we still have to implement it, try on the other Earth'
  _log.write('Spacecraft_&_Environment_Module -> Done\n')
  _log.write('That was easy, I had nothing to do\n')
  _log.write('\n')
    
else:

  print 'Skip Spacecraft & Environment Module...'

  _log.write('Spacecraft & Environment Module -> Skipped\n')
  _log.write('\n')

# ============================================================================================================

# =================== Read the OM Module =====================================================================

# Questo modulo qua dovrebbe aprire il database


if GCP.find("./Modules/Parameter/[@title='OM']/Value").text == 'T':

  print 'Run Optical Model Module...'

  _log.write('Run Optical Model Module...\n')
  
  print 'In this Universe we still have to implement it, try on the other Earth'
  _log.write('Optical Model Module -> Done\n')
  _log.write('That was easy, I had nothing to do\n')
  _log.write('\n')
    
else:

  print 'Skip Optical Model Module...'
  
  _log.write('Optical Model Module -> Skipped\n')
  _log.write('\n')

# ============================================================================================================

# =================== Read the DS Module =====================================================================

if GCP.find("./Modules/Parameter/[@title='DS']/Value").text == 'T':

  print 'Run Detection System Module...'

  _log.write('Run Detection System Module...\n')
  
  for point_id in pointing_list:
    
    print 'Running TIPS on pointing %d...'%point_id
    
    point_folder_TIPSoutput=os.path.join(GCP.find("./DirectoryNames/Parameter/[@title='StoringDirectory']/Value").text,'pointing%d'%point_id,GCP.find("./DirectoryNames/Parameter/[@title='TIPSOutputDirectory']/Value").text)
    os.mkdir(point_folder_TIPSoutput) if not os.path.exists(point_folder_TIPSoutput) else None
    
    current_pointing_id=np.where(opt['pointingID']==point_id)[0]
    
    _log.write('Pointing %d selected...\n'%point_id)
    _log.write('Setting up TIPS on pointing %d...\n'%point_id)

    # Since I have found some conflict between TISP (using aXe) and aXe I/O, I have to create a separate temporary file for the TIPS simulation

    tmp=open('tmpTIPS.py','w+')
    tmp.write('from SimNISP import NISP_S\n')
    tmp.write("nisp = NISP_S('%s',inDir='%s',outDir='%s')\n"%(GCP.find("./FileNames/Parameter/[@title='Catalog']/Value").text.replace('.fits','_TIPS.fits'), \
						      GCP.find("./FileNames/Parameter/[@title='Catalog']/Source").text, \
						      os.path.join(GCP.find("./DirectoryNames/Parameter/[@title='StoringDirectory']/Value").text,'pointing%d'%point_id,GCP.find("./DirectoryNames/Parameter/[@title='TIPSOutputDirectory']/Value").text)
						     )
             )
    tmp.write('nisp.get_pointing(%f,%f,%f)\n'%(opt['ra0'][current_pointing_id][0],opt['dec0'][current_pointing_id][0],opt['exptime'][current_pointing_id][0]))
    tmp.write("nisp.load_CustomConfig('%s')\n"%GCP.find("./FileNames/Parameter/[@title='TIPSConfiguration']/Value").text)
    tmp.write('nisp.run_Simulation()\n')
    tmp.close()
    
    _log.write('Running TIPS on pointing %s...\n'%point_id)
    _log.write('\n')

    os.system('python tmpTIPS.py >> %s 2>&1'%os.path.join(GCP.find("./DirectoryNames/Parameter/[@title='StoringDirectory']/Value").text,'pointing%d'%point_id,GCP.find("./DirectoryNames/Parameter/[@title='TIPSOutputDirectory']/Value").text,'TIPS.log'))
    os.remove('tmpTIPS.py')

  _log.write('Detection System Module Module -> Done\n')
  _log.write('TIPS correctly run\n')
  _log.write('\n')

  #sys.stdout=sys.__stdout__
  
else:

  print 'Skip Detection System Module...'

  _log.write('Detection System Module -> Skipped\n')
  _log.write('\n')

# ============================================================================================================

# =================== Read the OBDG Module ===================================================================

if GCP.find("./Modules/Parameter/[@title='OBDG']/Value").text == 'T':

  print 'Run On-Board Data Generation Module...'

  _log.write('Run On-Board Data Generation Module...\n')

  print 'In this Universe we still have to implement it, try on the other Earth'
  _log.write('On-Board Data Generation Module -> Done\n')
  _log.write('That was easy, I had nothing to do\n')
  _log.write('\n')
  
else:

  print 'Skip On-Board Data_Generation Module...'

  _log.write('On-Board_Data Generation Module -> Skipped\n')
  _log.write('\n')

# ============================================================================================================

# =================== Read the DPC Module ====================================================================

if GCP.find("./Modules/Parameter/[@title='DPC']/Value").text == 'T':
  
  print 'Run Data Processing & Calibration Module...'

  _log.write('Run Data Processing & Calibration Module...\n')
  
  ST=SpectralTools()
  
  _log.write('\n')
  _log.write('Extracting beam sensitivity...\n')
  
  AF = aXeFiles(GCP.find("./FileNames/Parameter/[@title='Catalog']/Value").text, GCP.find("./FileNames/Parameter/[@title='TIPSConfiguration']/Value").text)
  
  AF.extract_beam_sensitivity('A')

  _log.write('\n')
  _log.write('Creating aXe configuration file (per dither)...\n')
  
  for dither_id in np.arange(4):
    AF.create_conf_file(dither_id)
    
  # create source list

  _log.write('Creating list of sources...\n')

  source_list = []
  for id_source in range(1,source_num+1):
    source_list.append('BEAM_%dA'%id_source)

  for pointing_id in pointing_list:  

    hdulist=[]
    hdulist.append(fits.PrimaryHDU())

    current_pointing_pos=np.where(opt['pointingID']==point_id)[0]

    _log.write('Pointing %d selected...\n'%pointing_id)
    _log.write('\n')
  
    for detector_id in detector_list:
      
      print 'Extracting spectra from pointing %d - detector %s...'%(pointing_id, detector_id)
      
      detector_folder=os.path.join(GCP.find("./DirectoryNames/Parameter/[@title='StoringDirectory']/Value").text,'pointing%d'%pointing_id,GCP.find("./DirectoryNames/Parameter/[@title='E2ESOutputDirectory']/Value").text,'DETECTOR_%s'%detector_id)
      # I clean the DETECTOR_XX directory to avoid a mess with the files
      shutil.rmtree(detector_folder) if os.path.exists(detector_folder) else None

      _log.write('Cleaning %s...\n'%detector_folder)      

      # I copy the dispersed images of a single detector in the same directory DETECTOR_XX in which they will be processed
      same_detector_disp_images = []
      rootdir = os.path.join(GCP.find("./DirectoryNames/Parameter/[@title='StoringDirectory']/Value").text,'pointing%d'%pointing_id,GCP.find("./DirectoryNames/Parameter/[@title='TIPSOutputDirectory']/Value").text,'OUTSIM')
      ST.select_files_from_folder(rootdir,keyword=detector_id)

      # Create the DETECTOR_XX directory
      os.mkdir(detector_folder) if not os.path.exists(detector_folder) else None  

      _log.write('Creating %s (if not present)...\n'%detector_folder)

      # Copy files from TIPS output to the DETECTOR_XX directory
      for ff in ST.file_list:
	shutil.copy(os.path.join(rootdir,ff),detector_folder)

	_log.write('Copy file %s from %s to %s...\n'%(ff,rootdir,detector_folder))
	
        # Infer ditherID from file name
	if ff.find('GRED0') != -1:
	  ditherID = 0
	elif ff.find('GRED90_0_') != -1:
	  ditherID = 1
	elif ff.find('GRED90_1_') != -1:
	  ditherID = 3
	elif ff.find('GRED180') != -1:
	  ditherID = 2
	
	# Create the .dat file for aXe   
	AF.create_dat(ditherID,opt['band'][current_pointing_pos],ff,directory=detector_folder,output_dat_name=ff.replace('.fits','.dat'))

	_log.write('Creating .dat file for %s...\n'%os.path.join(detector_folder,ff))

	# Running aXe
	_log.write('Running aXe on %s/%s...\n'%(detector_folder,ff))
      
	# cleaning pravious axe_output:

	prefix=os.path.join(detector_folder,ff).replace('.fits','_2')
	aXe_out_list=[prefix+'.cat', \
	              prefix+'.CONT.fits', \
	              prefix+'.OAF', \
	              prefix+'.PET.fits', \
	              prefix+'.SPC.fits' \
	             ]
	for out in aXe_out_list:
	  os.remove(out) if os.path.exists(out) else None

	# aXe seems to work only locally. Since we can store data in a remote folder we have to adjust the absolute path:
	
	if detector_folder.split('/')[0] == '':
	  tmp=detector_folder.split('/')
	  del tmp[0]
	  for i in range(len(tmp)):
	    tmp[i]='..'
	  relative='/'.join(tmp)	
	  relative_detector_folder=relative+detector_folder
	  os.system('sh ./auxiliary/axe/aXeRun.sh %s %s %s >> %s 2>&1'%(os.path.join(relative_detector_folder,ff), \
								        os.path.join(relative_detector_folder,ff.replace('.fits','.dat')), \
									os.path.join(GCP.find("./FileNames/Parameter/[@title='aXeConfiguration']/Source").text, 'axe_dither%d.conf'%dither_id), \
									os.path.join(relative_detector_folder,ff.replace('.fits','.aXe.log')) \
								        )
		   )
	else:
	  os.system('sh ./auxiliary/axe/aXeRun.sh %s %s %s >> %s 2>&1'%(os.path.join(detector_folder,ff), \
								        os.path.join(detector_folder,ff.replace('.fits','.dat')), \
									os.path.join(GCP.find("./FileNames/Parameter/[@title='aXeConfiguration']/Source").text, 'axe_dither%d.conf'%dither_id), \
									os.path.join(detector_folder,ff.replace('.fits','.aXe.log')) \
								        )
		   )



    
      # find lines and calculate redshift (as Halpha). The output is stored in fits files
      # select files from detector folder    
      for source in source_list:
	to_combine=[]
	for _file in ST.file_list:	  
	  spec_file = _file.replace('.fits','_2.SPC.fits')
	  try:
	    ST.select_spectrum_from_file(_file.replace('.fits','_2.SPC.fits'),source,directory=detector_folder)
	    to_combine.append(ST.single_spectrum)
	  except:
	    pass 
	if len(to_combine) != 0:
	  ST.combine_multiple_spectra(to_combine)
	  ST.load_spectrum()
	  if len(ST.spectrum[2]) >1:
	    
	    print 'Processing pointing %d - detector %s - source %s...'%(pointing_id,detector_id,source)
	    _log.write('Processing pointing %d - detector %s - source %s...\n'%(pointing_id,detector_id,source))
	    
            ST.fast_smooth_spectrum()
	    ST.peak_finder()
	    ST.get_lines()
	    ST.calculate_redshift_as_Halpha()
	    ST.match_lines()
	    if ST.z_list != ():
	      hdulist.append(fits.ImageHDU(ST.z_list[1],name=ST.z_list[0]+'_DET%s'%detector_id))
	      hdulist[-1].header.set('Author','E. Romelli')	      
	      hdulist[-1].header.set('Date',time.ctime())	      
	      hdulist[-1].header.set('Pointing',pointing_id)
	      hdulist[-1].header.set('Detector',detector_id)
    thdulist=fits.HDUList(hdulist)
    thdulist.writeto(os.path.join(GCP.find("./DirectoryNames/Parameter/[@title='StoringDirectory']/Value").text,'pointing%d'%pointing_id,'RESULTS.redshift_measures.fits'),clobber=True)	    

    _log.write('Data Processing & Calibration Module -> Done\n')
    _log.write('\n')
    _log.write('\n')
  
else:

  print 'Skip Data Processing & Calibration Module...'

  _log.write('Data_Processing & Calibration Module -> Skipped\n')
  _log.write('\n')

# ============================================================================================================

# =================== Read the PA Module ===================================================================

if GCP.find("./Modules/Parameter/[@title='PA']/Value").text == 'T':

  print 'Run Performance Assessment Module...'

  _log.write('Run Performance Assessment Module...\n')

  # Create the PA Module directory in witch the simulator stores the output
  plot_folder = os.path.join(GCP.find("./DirectoryNames/Parameter/[@title='StoringDirectory']/Value").text,GCP.find("./DirectoryNames/Parameter/[@title='PerformanceAssessmentOutputDirectory']/Value").text)
  os.mkdir(plot_folder) if not os.path.exists(plot_folder) else None

  _log.write('Creating %s (if not present)...\n'%plot_folder)

  PA=PerformanceAssessment(pointing_list, \
			   GCP.find("./FileNames/Parameter/[@title='Catalog']/Value").text, \
			   PA_output_directory=plot_folder, \
			   catalog_directory = GCP.find("./FileNames/Parameter/[@title='Catalog']/Source").text, \
			   store_directory = GCP.find("./DirectoryNames/Parameter/[@title='StoringDirectory']/Value").text)
  PA.plot_z_per_pointing()
  PA.plot_hits()
  PA.plot_matches()


  print 'Plotting...'
  _log.write('Plotting...\n')

  PA.evaluate_statistics('RESULTS.Simulation_Statistics.csv')
 
  print 'Evaluating statistics...'
  _log.write('Evaluating statistics...\n')
  
  _log.write('Performance Assessment Module -> Done\n')
  _log.write('\n')
  
else:

  print 'Skip Performance Assessment Module...'

  _log.write('Performance Assessment Module -> Skipped\n')
  _log.write('\n')

# ============================================================================================================

print ''
print "And that's all folks!"
_log.write("And that's all folks!\n")
_log.write('\n')
_log.close()

