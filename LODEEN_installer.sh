#! /bin/bash

read -s -p 'Your password is required! PROVIDE! PROVIDE!' PASSWORD

sudo -k
if sudo -lS &> /dev/null << EOF
$PASSWORD
EOF
then
    echo
    echo 'Your password is correct!'
else 
    echo
    echo 'Your password is not correct! EXPLAIN! EXPLAIN!'
    exit
fi

echo
echo '== STARTING INSTALLATION =='
echo
echo '== TIPS INSTALLATION =='
echo

echo 'Compiling wcstools...'
echo
cp ./proto-E2ES_v2.0/tar_files/wcstools-3.9.2.tar.gz .
tar -zxf wcstools-3.9.2.tar.gz
rm wcstools-3.9.2.tar.gz
cd wcstools-3.9.2
make -s
echo

# Exit from wcstools-3.9.2 folder
cd

echo 'Compiling cfitsio...'
echo
cp ./proto-E2ES_v2.0/tar_files/cfitsio3370.tar.gz .
tar -zxf cfitsio3370.tar.gz
rm cfitsio3370.tar.gz
cd cfitsio
./configure -q
make -s
make install -s
echo

# Exit from cfitsio folder
cd

echo 'Installing asciidata...'
echo
cp ./proto-E2ES_v2.0/tar_files/asciidata-1.1.1.tar.gz .
tar -zxf asciidata-1.1.1.tar.gz
rm asciidata-1.1.1.tar.gz
cd asciidata-1.1.1
echo $PASSWORD | sudo -S python setup.py install
echo

# Exit from asciidata-1.1.1 folder
cd

echo 'Compiling TIPS...'
echo
cp ./proto-E2ES_v2.0/tar_files/tips_codeen.tar.gz .
tar -zxf tips_codeen.tar.gz
rm tips_codeen.tar.gz
cd ./tips_codeen/axesim
./configure -q --with-cfitsio-prefix=/home/user/cfitsio --with-wcstools-prefix=/home/user/wcstools-3.9.2
make -s
echo

# Exit from tips_codeen folder
cd

pwd
echo
echo '== PROTO-E2ES SETUP =='
echo
echo 'Configuring environment...'
echo
cd ./proto-E2ES_v2.0
sed -i -e "s/dottorando/$USERNAME/" set_environment.source


echo '== INSTALLATION COMPLETED =='
echo





