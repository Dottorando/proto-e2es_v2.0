class PerformanceAssessment:
  
  def __init__(self,pointing_list,catalog_name,PA_output_directory='./', catalog_directory = './catalog/',store_directory='./'):
    
    import numpy as np
    import os
    
    self.pointing_list = pointing_list
    self.sigma2fwhm = 2*np.sqrt(2*np.log(2.))
    self.PA_output_directory=PA_output_directory
    self.catalog = os.path.join(catalog_directory,catalog_name)
    self.store_directory=store_directory
    #self.E2ES_output_directory=E2ES_output_directory

  def plot_hits(self, color_list=['b','r','m','k']):
    
    import numpy as np
    import matplotlib.pyplot as plt
    from astropy.io import fits
    import os
        
    cat=fits.open(self.catalog)

    plt.figure()
    plt.plot(cat[1].data['alpha'],cat[1].data['delta'],'co') #This will change accordingly to the data model

    for pointingID in self.pointing_list:
      hit = []
      fresults = os.path.join(self.store_directory,'pointing%d'%pointingID,'RESULTS.redshift_measures.fits')
      results=fits.open(fresults)
      for i in range(1,len(results)):
	# I append the source number lowered by one so I store the index in the vector to take the correct coordinates
	hit.append(int(results[i].header['EXTNAME'].split('_')[1][:-1])-1)
      hit=np.unique(hit)
      plt.plot(cat[1].data['alpha'][hit],cat[1].data['delta'][hit],'.',c=color_list[pointingID-1])

    plt.xlabel('RA [deg]')
    plt.ylabel('DEC [deg]')
    plt.title('Hits')
    plt.savefig(os.path.join(self.PA_output_directory,'RESULTS.plot_hit.png'))


  def plot_z_per_pointing(self, color_list=['b','r','m','c']):
    
    import numpy as np
    import matplotlib.pyplot as plt
    import os
    from astropy.io import fits
    from collections import OrderedDict
    
    cat=fits.open(self.catalog)

    plt.figure()

    for pointingID in self.pointing_list:
      fresults = os.path.join(self.store_directory,'pointing%d'%pointingID,'RESULTS.redshift_measures.fits')
      results=fits.open(fresults)
      for i in range(1,len(results)):
	#for l in ff[i].data:
	source=int(results[i].header['EXTNAME'].split('_')[1][:-1])
	plt.errorbar(np.ones(len(results[i].data))*source,results[i].data[:,2],yerr=results[i].data[:,3],fmt='.',c=color_list[pointingID-1],label='pointing%d'%pointingID)

    # A little trick to avoid redundancy in the legend because I am labelling within a loop
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))

    plt.plot(cat[1].data['num'],cat[1].data['z'],'k-')
    plt.xlim((-1,len(cat[1].data['num'])+1))
    plt.xlabel('Source ID')
    plt.ylabel('Redshift')
    plt.title('Pointings')
    plt.legend(by_label.values(), by_label.keys(),loc=1,ncol=2)
    plt.savefig(os.path.join(self.PA_output_directory,'RESULTS.plot_pointings.png'))
    
  def plot_matches(self, directory='./'):
    
    import numpy as np
    import matplotlib.pyplot as plt
    import os
    from astropy.io import fits
    from collections import OrderedDict
    
    cat=fits.open(self.catalog)

    plt.figure()
    
    for pointingID in self.pointing_list:
      fresults = os.path.join(self.store_directory,'pointing%d'%pointingID,'RESULTS.redshift_measures.fits')
      results=fits.open(fresults)
      for i in range(1,len(results)):
	for l in results[i].data:
	  if len(results[i].data) == 1 and l[4] == 1:
	    plt.errorbar(int(results[i].header['EXTNAME'].split('_')[1][:-1]),l[2],yerr=l[3],fmt='o',c='r',label='len=1,match=1')
	  elif len(results[i].data) == 1 and l[4] > 1:
	    plt.errorbar(int(results[i].header['EXTNAME'].split('_')[1][:-1]),l[2],yerr=l[3],fmt='o',c='b',label='len=1,match>1')
	  elif len(results[i].data) > 1 and l[4] == 1:
	    plt.errorbar(int(results[i].header['EXTNAME'].split('_')[1][:-1]),l[2],yerr=l[3],fmt='s',c='y',label='len>1,match=1')
	  elif len(results[i].data) > 1 and l[4] > 1:
	    plt.errorbar(int(results[i].header['EXTNAME'].split('_')[1][:-1]),l[2],yerr=l[3],fmt='s',c='g',label='len>1,match>1')
	  else:
	    plt.errorbar(int(results[i].header['EXTNAME'].split('_')[1][:-1]),l[2],yerr=l[3],fmt='x',c='k',label='negative_fwhm')	  

    # A little trick to avoid redundancy in the legend because I am labelling within a loop
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))

    plt.plot(cat[1].data['num'],cat[1].data['z'],'k-')
    plt.xlim((-1,len(cat[1].data['num'])+1))
    plt.xlabel('Source ID')
    plt.ylabel('Redshift')
    plt.title('Matches')
    plt.legend(by_label.values(), by_label.keys(),loc=4,ncol=1)
    plt.savefig(os.path.join(self.PA_output_directory,'RESULTS.plot_matches.png'))
    
  def evaluate_statistics(self, file_name, mission_requirements_file='scientific_requirements.csv', mission_requirements_directory='./auxiliary/Performance_Assessment_Module', reqs_typelist=[float,float,float,float,float]):
    
    import numpy as np
    import matplotlib.pyplot as plt
    import os
    from astropy.io import fits
    
    cat=fits.open(self.catalog)

    reqs = np.genfromtxt(open(os.path.join(mission_requirements_directory,mission_requirements_file),'r'),delimiter=',',names=True,dtype=reqs_typelist)
            
    #to_search = np.arange(1,self.N+1)
    #to_search = to_search.astype(str)
    
    stat_file = open(os.path.join(self.PA_output_directory,file_name),'w+')
    
    header = 'completeness,'    
    header+= 'completeness_requirement,'
    header+= 'purity_partial,'
    header+= 'purity,'
    header+= 'purity_requirement,'
    header+= 'sigma_z_min,'
    header+= 'sigma_z_max,'
    header+= 'sigma_z_mean,'
    header+= 'sigma_z_min_requirement,'
    header+= 'sigma_z_max_requirement'
    
    stat_file.write(header+'\n')

    source_list = np.arange(1,len(cat[1].data['num'])+1)
    hit = []
    purity=[]
    purity_par=[]
    sigma_z=[]
    for pointingID in self.pointing_list:
      fresults = os.path.join(self.store_directory,'pointing%d'%pointingID,'RESULTS.redshift_measures.fits')
      results=fits.open(fresults)
      for source in source_list:
	# find all the hits
        for i in range(1,len(results)):
	  if results[i].header['EXTNAME'].find('%dA'%source) != -1:
	    hit.append(source)
	  # find all the correctly detected sources
	    if len(results[i].data)==1:
	      l=results[i].data[0]
	      sigma_z.append(l[3])
	      if abs(l[2]-cat[1].data['z'][source-1])<=abs(2*l[3]):
		purity_par.append(source)
		purity.append(source)
	    else:
	      for l in results[i].data:
		sigma_z.append(l[3])
		if abs(l[2]-cat[1].data['z'][source-1])<=abs(2*l[3]):
		  purity.append(source)
    hit=np.unique(hit)
    purity=np.unique(purity)
    purity_par=np.unique(purity_par)

    line = '%f,'%(len(hit)/float(len(cat[1].data['num'])))  
    line+= '%s,'%reqs['completeness']
    line+= '%f,'%(len(purity_par)/float(len(hit)))    
    line+= '%f,'%(len(purity)/float(len(hit)))
    line+= '%s,'%reqs['purity']            
    line+= '%f,'%min(sigma_z)
    line+= '%f,'%max(sigma_z)
    line+= '%f,'%np.mean(sigma_z)
    line+= '%f,'%(float(reqs['z_error'])*(1+float(reqs['z_min'])))
    line+= '%f,'%(float(reqs['z_error'])*(1+float(reqs['z_max'])))
    
    stat_file.write(line+'\n')
    stat_file.close()
  