class NISP_S:

  def __init__(self,input_file_name, inDir = './', outDir = './', inSpcForm = 'SplitFits', inCatForm = 'TIPS', inThmForm = 'SplitFits'):

    import os

    self.input_file = os.path.join(inDir, input_file_name)
    self.outDir = outDir
    self.inSpcForm = inSpcForm
    self.inCatForm = inCatForm
    self.inThmForm = inThmForm
  
  def get_pointing(self,ra0, dec0, exptime):
    
    self.ra0 = ra0
    self.dec0 = dec0
    self.exptime = exptime

  def load_TipsEuclidDefault(self, grismName):
    
    import tips

    self.grismName = grismName
    self.obs = tips.Observation(self.input_file, inSpcForm=self.inSpcForm, inCatForm=self.inCatForm, inThmForm=self.inThmForm, silent=False)
    self.obs.loadEUCLIDDefault(grismName=self.grismName, exptime=self.exptime, ra0=self.ra0, dec0=self.dec0)

  def load_CustomConfig(self,config_file_name,conf_dir = './conf'):
    
    import os
    import tips
    from astropy.io import fits

    self.config_file = os.path.join(conf_dir,config_file_name)
    self.obs = tips.Observation(self.input_file, inSpcForm=self.inSpcForm, inCatForm=self.inCatForm, inThmForm=self.inThmForm, silent=False)
    self.obs.loadFromFile(self.config_file,exptime=self.exptime,ra0=self.ra0, dec0=self.dec0, x0 = 1.0, y0 = 1.0)

  def run_Simulation(self):
    self.obs.runSimulation(workDir=self.outDir)

class NISP_P:
  
  def __init__(self):
    
    pass
