class SpectralTools:
  
  def __init__(self):
    
    import numpy as np

    self.sigma2fwhm=2*np.sqrt(2*np.log(2))

  def gaussian(self,x,I,x0,sigma,bkg):
    
    import numpy as np
    
    return I*np.exp(-((x-x0)/sigma)**2/2.)+bkg
    #return I*np.exp(-((x-x0)**2/sigma)/2.)+bkg
    
  def straight_line(self,x,a,b):
    
    return a*x+b
  
  def fast_smoothing(self,data):
    
    import numpy as np
    
    data_left=np.roll(data,1)
    data_left[0]=data[0]
    data_right=np.roll(data,-1)
    data_right[-1]=data[-1]
    return (data_left+data_right+data)/3.

  def triangular_smoothing(self,data):
    
    import numpy as np
    
    sum_data=3*data
    coeff=2
    for n in range(1,3):
      tmpl=np.roll(data,n)
      tmpr=np.roll(data,-n)
      for i in range(n):
	tmpl[i]=data[i]
	tmpr[-(i+1)]=data[-(i+1)]
      sum_data+=coeff*tmpl
      sum_data+=coeff*tmpr
      coeff-=n
    return sum_data/9.  

  def convolved_smoothing(self,data, box_len):
    
    import numpy as np

    box = np.ones(box_len)/box_len
    data_smooth = np.convolve(data, box, mode='same')
    for i in np.arange(box_len/2,0,-1):
      data_smooth[i-1]=data_smooth[i]
      data_smooth[-i]=data_smooth[-(i+1)]
    return data_smooth
    
  def select_files_from_folder(self,rootdir,keyword=None):
    
    import os
    
    self.rootdir=rootdir
    self.keyword=keyword
    
    self.file_list = []
    if self.keyword == None:
      for subdir, dirs, files in os.walk(self.rootdir):
	for ff in files:
	  self.file_list.append(ff)
    else:
      for subdir, dirs, files in os.walk(self.rootdir):
	for ff in files:
	  if ff.find(self.keyword) != -1:
	    self.file_list.append(ff.split('/')[-1])
    
  def select_spectrum_from_file(self,file_name, spectrum_ID, directory = './'):
    
    import astropy.io.fits as fits
    import numpy as np
    import os

    self.file_name=os.path.join(directory,file_name)
    self.spectrum_ID=spectrum_ID

    ff=fits.open(self.file_name)
    self.single_spectrum=(self.spectrum_ID,ff[self.spectrum_ID].data['LAMBDA'],ff[self.spectrum_ID].data['FLUX'])
  
  def combine_multiple_spectra(self,spectra_list):

    import numpy as np
    import astropy.io.fits as fits
    from copy import deepcopy
    
    self.spectra_list=spectra_list
    
    sorted(self.spectra_list,key = lambda s: len(s[1]), reverse=True)
    sum_spec = deepcopy(self.spectra_list[0][2])
    for i_spec in range(1,len(self.spectra_list)):
      if len(self.spectra_list[i_spec][2])>1:
        finterp=np.interp(self.spectra_list[0][1],self.spectra_list[i_spec][1],self.spectra_list[i_spec][2])
        sum_spec+=finterp
    sum_spec = sum_spec/float(len(self.spectra_list))
    self.combined_spectrum=(self.spectra_list[0][0]+'_combined',self.spectra_list[0][1],sum_spec)

  def load_spectrum(self, combined=True):
    
    if combined:
      self.spectrum=self.combined_spectrum
    else:
      self.spectrum=self.single_spectrum
  
  def fast_smooth_spectrum(self):
    
    self.spectrum=(self.spectrum[0]+'_smoothed',self.spectrum[1],self.fast_smoothing(self.spectrum[2]))

  def triangular_smooth_spectrum(self):
    
    self.spectrum=(self.spectrum[0]+'_smoothed',self.spectrum[1],self.triangular_smoothing(self.spectrum[2]))
  
  def convolve_smooth_spectrum(self,box_len=3):
    
    self.box_len=box_len
    self.spectrum=(self.spectrum[0]+'_smoothed',self.spectrum[1],self.convolved_smoothing(self.spectrum[2],self.box_len))
      
  def continuum_subtraction(self):
    
    import numpy as np
    import math
    import warnings
    warnings.simplefilter('ignore', np.RankWarning)
    
    reference_order_of_magnitude=round(math.log10(abs(np.polyfit(self.spectrum[1],self.spectrum[2],0)[0])))
    order=1
    while round(math.log10(abs(np.polyfit(self.spectrum[1],self.spectrum[2],order)[0]))) > (reference_order_of_magnitude-9.):
      coefficients=np.polyfit(self.spectrum[1],self.spectrum[2],order)
      poly=np.poly1d(coefficients)
      order+=1

    self.poly_order = order
    self.continuum=(self.spectrum[0]+'_cont_profile',self.spectrum[1],poly(self.spectrum[1]))
    self.spectrum=(self.spectrum[0]+'_cont_subtracted',self.spectrum[1],self.spectrum[2]-poly(self.spectrum[1]))
  
  #def get_flux_threshold(self):
    
    #import numpy as np
    #idx=np.where(self.spectrum[2]<self.spectrum[2].mean())[0]
    #self.flux_threshold=self.spectrum[2][idx].mean()+self.spectrum[2][idx].std()
  
  def peak_finder(self):
    
    import numpy as np
    from scipy.optimize import curve_fit
        
    #self.flux_threshold=flux_threshold
    
    self.derivative=[]
    for i in range(1,len(self.spectrum[2])):
      self.derivative.append((self.spectrum[2][i]-self.spectrum[2][i-1]))#/(l[i]-l[i-1]))
    #smooth_der = self.fast_smoothing(self.derivative)
    
    idx=np.where(self.spectrum[2]<np.mean(self.spectrum[2]))[0]
    popt,pcov=curve_fit(self.straight_line,self.spectrum[1][idx],self.spectrum[2][idx])

    self.peaks=[]
    for i in range(1,len(self.derivative)):
      if (np.sign(self.derivative[i-1])>np.sign(self.derivative[i])) and ((self.spectrum[2]-self.straight_line(self.spectrum[1],popt[0],popt[1]))[i] > self.spectrum[2][idx].std()):
	self.peaks.append(i)

  def line_identification(self,peak_id,fit_maxfev):

    import numpy as np
    from scipy.optimize import curve_fit 

    #self.peak_id=peak_id
    self.identified_line=()
    
    left=-np.inf#0
    right=np.inf#len(self.smoothed_spectrum[2])
    
    for pnt in range(peak_id,0,-1):
      if self.spectrum[2][pnt]-self.spectrum[2][pnt-1] < 0.:
	left=pnt
	break

    for pnt in range(peak_id,len(self.spectrum[2])-1):
      if self.spectrum[2][pnt]-self.spectrum[2][pnt+1] < 0.:
	right=pnt
	break
    
    # Se sono ai bordi non mi interessa!
    if right-left > 10 and left != -np.inf and right != np.inf:
      id_cut=np.arange(left,right)
      cut_spectrum=self.spectrum[2][id_cut]
      cut_lambdas=self.spectrum[1][id_cut]
      guess_on_sigma=(self.spectrum[1][right]-self.spectrum[1][left])/2./self.sigma2fwhm
      popt,pcov=curve_fit(self.gaussian,cut_lambdas,cut_spectrum,p0=[self.spectrum[2][peak_id],self.spectrum[1][peak_id],guess_on_sigma,self.spectrum[2].min()],maxfev=fit_maxfev)
      self.identified_line+=(self.spectrum[0], popt)      
      
  def get_lines(self,fit_maxfev=5000):
    
    import numpy as np
    
    self.fit_maxfev=fit_maxfev
    self.spectral_lines=()

    lines=[]
    for p in self.peaks:
      self.line_identification(p,fit_maxfev)
      if self.identified_line != (): # and (self.identified_line[1][0]+self.identified_line[1][3]) > self.flux_threshold and abs(self.identified_line[1][2])<100.:
	lines.append(self.identified_line[1])
    if lines != []:
      # lines can be empty because we can have a spectrum with no line detected and so all the identified_line==() and the code do not enters the preavious if condition
      self.spectral_lines+=(self.spectrum[0],np.array(lines))
    
  def calculate_redshift_as_Halpha(self, unit='angstrom'):
    
    import numpy as np

    self.unit=unit
    self.z_list=()

    if unit=='angstrom':
      self.Halpha=6562.8      
    elif unit=='nanometer':
      self.Halpha=656.28
    elif unit=='micron':
      self.Halpha=0.65628
    else:
      print '''WARNING! unit not allowed! Allowed value for unit:
            - angstrom
            - nanometer
            - micron'''
    
    if self.spectral_lines != ():
      z=np.zeros(self.spectral_lines[1].shape)
      z[:,0]=self.spectral_lines[1][:,1]
      z[:,1]=self.spectral_lines[1][:,2]
      z[:,2]=self.spectral_lines[1][:,1]/self.Halpha-1.
      z[:,3]=self.spectral_lines[1][:,2]/self.Halpha
      
      self.z_list+=(self.spectral_lines[0],z)

  def match_lines(self, reference_lines_file='Righe.csv', directory='./auxiliary/redshift_extraction'):
    
    import numpy as np
    import os
    
    self.reference_lines_file=os.path.join(directory,reference_lines_file)
    
    tmp = np.genfromtxt(open(self.reference_lines_file,'r'),delimiter=',',names=True,dtype=['|S5',float])
    ref_lines_rest=tmp['lambda']
    
    if self.z_list != ():
      number_of_measured_lines=len(self.z_list[1])
      match_column=np.zeros(number_of_measured_lines)
      reliability_column=np.zeros(number_of_measured_lines)
      for iz in range(number_of_measured_lines):
	match=0
	ref_lines_shifted=ref_lines_rest*(1+self.z_list[1][iz][2])
	for il in range(number_of_measured_lines):
	  for rl in ref_lines_shifted:
	    # I take the abs of the fwhm of the line because some spectra have the lambda reversed so right-left is not positive
	    if abs(rl-self.z_list[1][il][0]) <= 2*abs(self.z_list[1][il][1]):
	      match+=1
	match_column[iz]=match
	reliability_column[iz]=float(match)/number_of_measured_lines
      
      z_with_match=np.hstack([self.z_list[1],match_column.reshape(number_of_measured_lines,1),reliability_column.reshape(number_of_measured_lines,1)])
      self.z_list=()
      self.z_list=(self.spectral_lines[0],z_with_match)
