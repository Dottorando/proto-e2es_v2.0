import os
import numpy as np
import random

class CatalogHandler:

  def __init__(self, catalog_file_name, directory = './catalog'):

    import os
    from astropy.io import fits

    self.directory=directory
    self.catalog_path = os.path.join(directory,catalog_file_name)
    self.catalog = fits.open(self.catalog_path) 

  def coordRange(self, amin, amax, dmin, dmax):
    """ Select the objects inside a given coordinates range """
    
    import numpy as np
    
    self.coord_indexes = np.where((self.catalog[1].data['alpha']>=amin) & (self.catalog[1].data['alpha']<=amax) & (self.catalog[1].data['delta']>=dmin) & (self.catalog[1].data['delta']<=dmax))[0]
    
  def magRange(self, mmin, mmax):
    """ Select the objects inside a given magnitude range """

    import numpy as np
    
    self.mag_indexes = np.where((self.catalog[1].data['mag']>=mmin) & (self.catalog[1].data['mag']<=mmax))[0]

  def zRange(self, zmin, zmax):
    """ Select the objects inside a given redshift range """

    import numpy as np
    
    self.z_indexes = np.where((self.catalog[1].data['z']>=zmin) & (self.catalog[1].data['z']<=zmax))[0]
    #self.allRange()
    
  def fluxRange(self, fmin, fmax):
    """ Select the objects inside a given line flux range """

    import numpy as np
    
    self.flux_indexes = np.where((self.catalog[1].data['fluxha']>=fmin) & (self.catalog[1].data['fluxha']<=fmax))[0]

  def joinRanges(self):
    """ Utility function to make the actual object selection"""
    
    import numpy as np
    
    N=4
    
    aux = np.sort(np.concatenate((self.coord_indexes,self.mag_indexes,self.z_indexes,self.flux_indexes), axis=1)) 
    shift = N-1
    self.selected_indexes = (aux[aux[shift:] == aux[:-shift]],) 

  def create_TIPS_input_catalog(self,amin=-999.,amax=999.,dmin=-999.,dmax=999.,minmag=-999.,maxmag=999.,zmin=-999.,zmax=999.,fmin=-999.,fmax=999.):
    
    # NOTE: the file is created following the logic of the 1.0 version
    
    import os
    import numpy as np
    from astropy.io import fits
    
    # select the intersection of the ranges of coordinates, magnitudes, z and fluxes    
    self.coordRange(amin,amax,dmin,dmax)
    self.magRange(minmag,maxmag)
    self.zRange(zmin,zmax)
    self.fluxRange(fmin,fmax)
    self.joinRanges()

    hdulist=[]
    hdulist.append(fits.PrimaryHDU())

    # create modimg vector
    tmp=np.ones(len(self.catalog[1].data['bt']))
    iext=np.where(self.catalog[1].data['bt']>=0.)
    tmp[iext]=0.0
    # create modfile and imgfile vectors
    path_to_spec=np.array([])
    path_to_img=np.array([])
    for n in self.catalog[1].data['num']:
      path_to_spec=np.append(path_to_spec,os.path.join(self.directory,'incident','000','incident_%d.fits'%n))
      path_to_img=np.append(path_to_img,os.path.join(self.directory,'images','000','image_%d.fits'%n))
    
    number=fits.Column(name='NUMBER',format='J',array=self.catalog[1].data['num'][self.selected_indexes])
    modspec=fits.Column(name='MODSPEC',format='K',array=np.ones(len(self.catalog[1].data['num']))[self.selected_indexes])
    modimg=fits.Column(name='MODIMG',format='K',array=tmp[self.selected_indexes])
    ra=fits.Column(name='RA',format='D',array=self.catalog[1].data['alpha'][self.selected_indexes])
    dec=fits.Column(name='DEC',format='D',array=self.catalog[1].data['delta'][self.selected_indexes])
    a_sky=fits.Column(name='A_SKY',format='D',array=np.ones(len(self.catalog[1].data['num']))[self.selected_indexes]) # unused by TIPS
    b_sky=fits.Column(name='B_SKY',format='D',array=np.ones(len(self.catalog[1].data['num']))[self.selected_indexes]) # unused by TIPS
    theta_sky=fits.Column(name='THETA_SKY',format='D',array=np.zeros(len(self.catalog[1].data['num']))[self.selected_indexes])
    modfile=fits.Column(name='MODFILE',format='40A',array=path_to_spec[self.selected_indexes])
    imgfile=fits.Column(name='IMGFILE',format='35A',array=path_to_img[self.selected_indexes])
    
    cols=fits.ColDefs([number,modspec,modimg,ra,dec,a_sky,b_sky,theta_sky,modfile,imgfile])
    hdulist.append(fits.BinTableHDU.from_columns(cols))
    thdulist=fits.HDUList(hdulist)
    thdulist.writeto(self.catalog_path.replace('.fits','_TIPS.fits'),clobber=True)
    
    
    
