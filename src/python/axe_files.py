class aXeFiles:

  def __init__(self, catalog_file_name, tips_conf_file_name, catalog_directory='./catalog', conf_directory='./conf'):
    
    import os

    self.catalog_file = os.path.join(catalog_directory,catalog_file_name)
    self.tips_conf_file = os.path.join(conf_directory,tips_conf_file_name)
    self.catalog_directory=catalog_directory
    self.conf_directory=conf_directory

  def load_band_filter(self, band, filter_file_name='Filters.csv', auxiliary_directory = './auxiliary/axe', typelist=['|S5',float,'|S5']):
    
    import numpy as np
    import os
    
    self.filter_file=os.path.join(auxiliary_directory, filter_file_name)
    
    filters=np.genfromtxt(open(self.filter_file,'r'),delimiter=',',names=True,dtype=typelist)
    selected=np.where(filters['band']==band)[0]
    return filters['band'][selected][0], filters['wavelength'][selected][0]
  
  def load_rotation(self, dither, rotation_file_name='DithersRot.csv', auxiliary_directory = './auxiliary/axe', typelist=['|S5',int,'|S5']):
    
    import numpy as np
    import os

    self.rotation_file=os.path.join(auxiliary_directory, rotation_file_name)
    rot=np.genfromtxt(open(self.rotation_file,'r'),delimiter=',',names=True,dtype=typelist)
    selected=np.where(rot['ditherID']==dither)[0]
    return rot['rot90'][selected][0]

  def get_EE80(self, DB_file_name = 'EUC-TEST-SPACESEGMENT-INSTRUMENT-NISP-NISPASDESIGNED.xml', auxiliary_directory = './auxiliary/Euclid_Mission_DB/SpaceSegment/Instrument/NISP/NISPAsDesigned'):
    
    from Mdb import Mdb
    import os
    import numpy as np
    
    self.DB_file=os.path.join(auxiliary_directory, DB_file_name)
    
    EE80_values = Mdb(self.DB_file).getValue('SpaceSegment.Instrument.NISP.NISPAsDesigned.NISPTotalSREE80PDR')
    wl=np.array([])
    EE=np.array([])
    for value in EE80_values:
      wl=np.append(wl,value[0])
      EE=np.append(EE,value[1])
    return wl, EE
  
  def extract_beam_sensitivity(self, beam_ID):
    
    from astropy.io import fits
    import os
    import sys
    
    tipsf = fits.open(self.tips_conf_file)
    primary_header = tipsf['PRIMARY'].header
    if beam_ID == 'A':
      bs_table = tipsf[primary_header['EXPO0']].header['SENS0']
    elif beam_ID == 'B':
      bs_table = tipsf[primary_header['EXPO0']].header['SENS1']
    else:
      print 'Error 404: beam %s not found! Check the TIPS configuration file! beam_ID must be a capital letter in string format'%beam_ID
      sys.exit()
    # Create a fits file containing only the beam sensitivity
    hdulist=[]
    hdulist.append(fits.PrimaryHDU())
    hdulist.append(tipsf[bs_table])
    thdulist=fits.HDUList(hdulist)
    new_file_name = os.path.join(self.conf_directory,bs_table+'.fits')
    thdulist.writeto(new_file_name,clobber=True)    
    
  def create_conf_file(self, dither):
    
    from astropy.io import fits
    import os
    
    self.dither = dither
    self.conf_file=os.path.join(self.conf_directory, 'axe_dither%d.conf'%self.dither)
    """
    Create the aXe configuration file
    """
    conff = open(self.conf_file,'w+')
    tipsf = fits.open(self.tips_conf_file)
    
    primary_header = tipsf['PRIMARY'].header
    dither_conf = tipsf[primary_header['EXPO%d'%self.dither]].header
    
    conff.write('INSTRUMENT DUMMY\n')
    conff.write('CAMERA DUMMY\n')
    conff.write('SCIENCE_EXT SCI\n')
    conff.write('DQ_EXT DQ\n')
    conff.write('ERRORS_EXT ERR\n')
    conff.write('FFNAME None\n')
    conff.write('RDNOISE %d\n'%primary_header['RN'])
    conff.write('EXPTIME EXPTIME\n')
    conff.write('POBJSIZE 0.5\n')
    conff.write('SMFACTOR 1.0\n')
    conff.write('#\n')
    #
    #beam
    #
    #order 1
    #
    conff.write('# Order 1 (BEAM %s)\n'%dither_conf['BEAMID0'])
    conff.write('#\n')
    conff.write('BEAMA %d %d\n'%(dither_conf['BSTART0'],dither_conf['BEND0']))
    conff.write('MMAG_EXTRACT_A 35\n')
    conff.write('MMAG_MARK_A 35\n')
    conff.write('#\n')
    conff.write('# Trace description\n')
    conff.write('#\n')
    conff.write('DYDX_ORDER_A %d\n'%dither_conf['ODYDX0'])
    for i in range(dither_conf['ODYDX0']+1):
      conff.write('DYDX_A_%d %g\n'%(i,dither_conf['DYDX0_%d' % i]))
    conff.write('#\n')
    conff.write('# X and Y Offsets\n')
    conff.write('#\n')
    conff.write('XOFF_A %f\n'%dither_conf['XOFF0'])
    conff.write('YOFF_A %f\n'%dither_conf['YOFF0'])
    conff.write('#\n')
    conff.write('# Dispersion solution\n')
    conff.write('#\n')
    conff.write('DISP_ORDER_A %d\n'%dither_conf['ODLDP0'])
    for i in range(dither_conf['ODLDP0']+1):
      conff.write('DLDP_A_%d %g\n'%(i,dither_conf['DLDP0_%d' % i]))
    conff.write('#\n')    
    conff.write('SENSITIVITY_A %s\n'%(os.path.join(self.conf_directory,dither_conf['SENS0']+'.fits')))
    conff.write('#\n')
    conff.close()

  def create_dat(self, dither, band, slitless_image_file, directory='./', output_dat_name = 'input.dat'):
    
    import tips
    import os
    import numpy as np
    from astropy.io import fits
    import csv
    #from catalog_handler import CatalogHandler
    #from Mdb import Mdb    

    self.slitless_image = os.path.join(directory,slitless_image_file)
    self.output_dat = os.path.join(directory,output_dat_name)

    # GET SLITLESS IMAGE INFOS
    slit_image_info = fits.open(self.slitless_image)[1].header
    
    conff=fits.open(self.tips_conf_file)   
    NPIX = conff[0].header['NPIXX']
    PIXSIZE = conff[0].header['PIXSIZE']

    wcs = tips.WCSObject()    
    wcs.updateWCS(pixel_scale=0.3, orient=0.0, \
                  refpos=[slit_image_info['CRPIX1'],slit_image_info['CRPIX2']], \
                  refval=[slit_image_info['CRVAL1'],slit_image_info['CRVAL2']], \
                  size=  [slit_image_info['NAXIS1'],slit_image_info['NAXIS2']])
    
    axinp = open(self.output_dat, 'w+')
    # start writing the catalog
    axinp.write('# 1  NUMBER\n')
    axinp.write('# 2  X_IMAGE\n')
    axinp.write('# 3  Y_IMAGE\n')
    axinp.write('# 4  X_WORLD\n')
    axinp.write('# 5  Y_WORLD\n')
    axinp.write('# 6  A_IMAGE\n')
    axinp.write('# 7  B_IMAGE\n')
    axinp.write('# 8  THETA_IMAGE\n')
    axinp.write('# 9  A_WORLD\n')
    axinp.write('# 10  B_WORLD\n')
    axinp.write('# 11  THETA_WORLD\n')
    
    # CARICO IL FILTRO
    b, w = self.load_band_filter(band)
    axinp.write('# 12  MAG_%s%4d\n' %(b, w)) # the first character of the filter name MUST be a magnitude label

    # GET THE EE80 FOR THE BAND WAVELENGHT
    wl, EE = self.get_EE80()
    EE80 = np.interp(w, wl, EE)
    
    #CARICO LA ROTAZIONE
    ditrot = self.load_rotation(dither)
    
    cat=fits.open(self.catalog_file)[1].data
    
    num=cat['num']
    ra=cat['alpha']
    dec=cat['delta']
    x_image_tmp,y_image_tmp = wcs.rd2xy(ra,dec)
    if ditrot == 'T':
      x_image = NPIX-y_image_tmp
      y_image = x_image_tmp
    else:
      x_image = x_image_tmp
      y_image = y_image_tmp
    mag = cat['mag']
    #template_name = cat['incident']
    #z = cat['z']
    rbulge = cat['rbulge']
    rdisk = cat['rdisk']
    bt = cat['bt']
    pa = cat['pa']
    #axratio = cat['axratio']    
    # point-like objects
    A = np.ones(len(cat['num']))*EE80/PIXSIZE
    B = A
    pa = np.zeros(len(cat['num']))
    # object dimensions
    id_ext=np.where(bt>=0)[0]
    obj_diam=2*np.sqrt(rbulge[id_ext]**2+(1-bt[id_ext])*rdisk[id_ext]**2) # arcsec
    obj_diam_convolved = np.sqrt(obj_diam**2+(np.ones(len(id_ext))*EE80*2)**2) # arcsec    
    A = obj_diam_convolved/PIXSIZE/2.0  # the extraction aperture is A * 1 * 2 (1 is mfwhm)
    B = A # * axratio - always a perpendicular extraction (no B = A*axratio)
    for i in range(len(num)):
      axinp.write('%d %f %f %f %f %.3f %.3f %.3f %.3f %.3f %.3f %.3f\n'%(num[i],x_image[i],y_image[i],ra[i],dec[i],A[i],B[i],pa[i],A[i],B[i],pa[i],mag[i])) 
    axinp.close()